# Introduction à la prédiction de séries temporelles

## Description
Dans ce projet vous apprendrez à utiliser le Machine Learning et Deep Learning pour la prédiction de séries temporelles.
En particulier, nous allons prédire la consommation totale du bâtiment GreEN-ER à l'aide de deux modèles simple, les LSTM et les Forêts aléatoires. 

## Description des fichiers
 - notebook.ipynb : notebook détaillant les principales étapes afin de réaliser une prédiction.

 - conso.csv : Fichier csv contenant la consommation énergétique totale du bâtiment GreEN-ER entre le 01-01-2020 et le 23-01-2022.

### Remarques

le notebook utilise les libraires suivante :
- matplotlib
- scipy 
- numpy
- tensorflow
- pandas 
- sklearn

Si vous ne les avez pas, il suffit de décommenter la première cellule du notebook et de l'exécuter, celle-ci installera automatiquement les libraires manquantes.
